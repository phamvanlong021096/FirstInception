class test_batchs:
    def __init__(self, data):
        self.data = data
        self.batch_index = 0

    def nextBatch(self, batch_size):
        if (batch_size + self.batch_index) > self.data.shape[0]:
            print("batch sized is messed up")
        batch = self.data[self.batch_index: (self.batch_index + batch_size), :, :, :]
        self.batch_index = self.batch_index + batch_size
        return batch